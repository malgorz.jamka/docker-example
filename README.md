# docker-example



## Zadanie 1: Docker


- Utwórz repozytorium, w którym będą znajdowały się pliki: 


  - requirements.txt z biblioteką pandas i scikit-learn 
  - plik app.py z naszego repozytorium.
  - Dockerfile, w którym:
    - Jako obrazu bazowego użyjesz Pythona w wersji 3.11,
    - Ustawisz katalog roboczy na /app
    - Skopiujesz plik requirements.txt do katalogu roboczego
    - Zainstalujesz zależności (podpowiedź: RUN pip install … )
    - Skopiujesz plik hello.py do katalogu roboczego w kontenerze
    - Zdefiniujesz instrukcję CMD, która uruchomi skrypt app.py podczas uruchomienia konteneru.


- Zbuduj obraz Docker, nazywając go ump_docker_twojeimienazwisko

- Uruchom kontener w oparciu o ten obraz

- Prześlij wszystkie pliki z repozytorium i komendy których użyłeś w kroku 2 i 3 🙂


# Zadanie 2: Poetry

- Utwórz kontener Docker analogiczny do poprzedniego zadania, ale zamiast używania pliku requirements.txt i instalowania zależności przez komendę pip, wykorzystaj Poetry.

### Podpowiedzi:

- W obrazie bazowym Pythona brakuje Poetry. Zainstaluj go za pomocą pip.
- Korzystając z Poetry w Dockerze, nie ma konieczności korzystania z oddzielnego wirtualnego środowiska. Aby je wyłączyć, przed zainstalowaniem zależności uruchom komendę

    `
    $ RUN poetry config virtualenvs.create false
    `
